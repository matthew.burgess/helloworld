terraform {
  required_version = "0.14.3"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.3.0"
    }
  }
}

variable "gitlab_token" {
  type        = string
  description = "The GitLab Personal Access Token (PAT) to use for authentication"
}

provider "gitlab" {
  token = var.gitlab_token
}

resource "gitlab_project" "helloworld" {
  name             = "helloworld"
  description      = "Spring Boot Hello World app"
  visibility_level = "public"
  merge_method     = "ff"
}
